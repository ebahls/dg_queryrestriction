/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation;

import dataglobal.dataroomauthorisation.model.JDBCQueryRestrictionImpl;
import dataglobal.queryrestrictionserver.QueryRestrictionPlugin;
import dataglobal.queryrestrictionserver.QueryRestrictionPluginFactory;
import dataglobal.queryrestrictionserver.Server;
import dataglobal.queryrestrictionserver.ServerFactory;
import dataglobal.queryrestrictionserver.security.HibernateCFGCredentialSecurityProvider;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.jdom2.JDOMException;

/**
 *
 * @author eobs
 */
public class JDBCDataRoomPluginFactory implements QueryRestrictionPluginFactory {

    static Logger log = LogManager.getLogger(JDBCDataRoomPluginFactory.class.getName());
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;
    private boolean grantUnknowenUser = false;

    @Override
    public QueryRestrictionPlugin getPlugin() {

        return new JDBCQueryRestrictionImpl(sessionFactory.openSession(), this.grantUnknowenUser);
    }

    @Override
    public void setup(HashMap<String, String> config) {

        //Hibernate init.
        try {

            HibernateCFGCredentialSecurityProvider provider = new HibernateCFGCredentialSecurityProvider(new File(config.get("datasource.file")));

            Configuration configuration = new Configuration().configure(provider.getDocument());

            String dllPath = configuration.getProperty("hibernate.connection.driver.dll");
            if (dllPath != null) {
                File dll = new File(dllPath);
                log.info("Load dll :" + dll.getParentFile().getAbsolutePath());

                
                Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
                fieldSysPath.setAccessible(true);
                fieldSysPath.set(null, null);
               System.setProperty("java.library.path", dll.getParentFile().getAbsolutePath());
               
                log.debug("lib path :" + System.getProperty("java.library.path"));
               // System.loadLibrary(dll.getName());
            }

            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            testConnection(configuration.getProperties());
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (HibernateException | IOException | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | InvalidKeyException ex) {
            log.error("Hibernate could not Initialized correctly cause:");
            log.error(ex);
            Server.stop(null);
        } catch (SQLException ex) {
            log.error("Hibernate database connection not correctly configured:");
            log.error(ex);
            Server.stop(null);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            log.error("Hibernate database connection not correctly configured Driver not found:");
            log.error(ex);
            Server.stop(null);
        } catch (JDOMException ex) {
            log.error("Hibernate database connection not correctly configured Driver not found:");
        } catch (IllegalBlockSizeException | BadPaddingException ex) {

            log.error("Hibernate database connection not correctly configured could not encrypt credentials:");
            log.error(ex);
            Server.stop(null);
        } catch (NoSuchFieldException ex) {
            java.util.logging.Logger.getLogger(JDBCDataRoomPluginFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            log.error("Hibernate database connection not correctly configured could not load the dll:");
            log.error(ex);
            Server.stop(null);
        }

        //Importer Init feed
        String cronexpr = config.getOrDefault("feedImporter.cron", "0 0 1 1/1 * ? *");
        String readerClass = config.getOrDefault("feedImporter.class", "noClass");
        ServerFactory.getInstance().addNewJob(cronexpr, readerClass, "FeedImporter", config);
        //just if the config says true
        if (config.getOrDefault("feedImporter.start.now","false").compareToIgnoreCase("true")==0)
        {
        ServerFactory.getInstance().fireJobNow("FeedImporter");
        }
        //Importer Init
        cronexpr = config.getOrDefault("groupimporter.cron", "0 0 1 1/1 * ? *");
        readerClass = config.getOrDefault("groupimporter.class", "noClass");
        ServerFactory.getInstance().addNewJob(cronexpr, readerClass, "GroupImporter", config);
        ServerFactory.getInstance().fireJobNow("GroupImporter");
        
        
        //setup
        this.grantUnknowenUser = Boolean.parseBoolean(config.getOrDefault("jdbcdataroom.grant.unknowen.user", "false"));

        log.info("Grante unknowen user access :" + this.grantUnknowenUser);

    }

    public static  synchronized Session getSession() {
        try
        {
             Session session= sessionFactory.openSession();
        return session;
        }
       catch (HibernateException ex)
       {
           log.error(ex);
       }
        return null;
        
    }

    private void testConnection(Properties p) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        String url = p.getProperty("hibernate.connection.url");
        String user = p.getProperty("hibernate.connection.username");
        String password = p.getProperty("hibernate.connection.password");
        Class.forName(p.getProperty("hibernate.connection.driver_class")).newInstance();

        DriverManager.getConnection(url, user, password).close();
    }

}
