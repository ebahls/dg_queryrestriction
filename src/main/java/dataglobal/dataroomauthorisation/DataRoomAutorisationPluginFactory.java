/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.dataroomauthorisation;

import dataglobal.queryrestrictionserver.DumyPluginImpl;
import dataglobal.queryrestrictionserver.QueryRestrictionPluginFactory;
import dataglobal.queryrestrictionserver.QueryRestrictionPlugin;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author eobs
 */
public class DataRoomAutorisationPluginFactory implements QueryRestrictionPluginFactory {

    static  Logger log = LogManager.getLogger(DataRoomAutorisationPluginFactory.class.getName());
    
    public DataRoomAutorisationPluginFactory() {
    }
    
     

  
    @Override
    public QueryRestrictionPlugin getPlugin() {
        return new DumyPluginImpl();
    }

    @Override
    public void setup(HashMap<String, String> config) {
        log.debug("Setup is done");
    }
    
}
