/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation.feed;

import dataglobal.dataroomauthorisation.JDBCDataRoomPluginFactory;
import static dataglobal.dataroomauthorisation.feed.CSVFeedReader.logaccs;
import dataglobal.dataroomauthorisation.model.database.Desk;
import dataglobal.dataroomauthorisation.model.database.Permissions;
import dataglobal.dataroomauthorisation.model.database.User;
import dataglobal.queryrestrictionserver.WindowsEventLogWriter;
import java.io.File;
import java.io.IOException;
import java.security.Permission;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author eobs
 */
@DisallowConcurrentExecution
public class XMLMappingFeedReader implements Job {

    static Logger log = LogManager.getLogger("XMLMappingFeedReader");
    static Logger logaccs = LogManager.getLogger("access");
    private Session session;
    private Hashtable<String, Desk> deskTable;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        this.session = JDBCDataRoomPluginFactory.getSession();

        String ouFilename;
        String userFilename;
        String path = context.getMergedJobDataMap().getString("feedImporter.in.path");

        //Checking explicite file name configuration
        if ((context.getMergedJobDataMap().getString("feedImporter.in.ou_mapping") == null)
                || (context.getMergedJobDataMap().getString("feedImporter.in.user_mapping") == null)) {
            logaccs.info("No explicit files declared for OU and User mapping");
            DateFormat dfmt = new SimpleDateFormat("YYYYMMdd");
            ouFilename = "OU_Mapping_" + dfmt.format(new Date()) + ".xml";
            userFilename = "User_Mapping_" + dfmt.format(new Date()) + ".xml";
            logaccs.info("Using for OU   Mapping : " + ouFilename);
            logaccs.info("Using for User Mapping : " + userFilename);

        } else {
            ouFilename = context.getMergedJobDataMap().getString("feedImporter.in.ou_mapping");
            userFilename = context.getMergedJobDataMap().getString("feedImporter.in.user_mapping");
            logaccs.info("Using for OU   Mapping : " + ouFilename);
            logaccs.info("Using for User Mapping : " + userFilename);
        }

        try {

            //cleanup(false);
            importUserMapping(new File(path, userFilename));
            importOU(new File(path, ouFilename));
            
            switchePassivToActive();
             WindowsEventLogWriter.fireWindowsEvent(WindowsEventLogWriter.SUCCESS, WindowsEventLogWriter.FEED_IMPORT_OK, "Feed Import OK");
            
        } catch (IOException | JDOMException ex) {
            log.error(ex);
            logaccs.error("Cant Import mapping");
            WindowsEventLogWriter.fireWindowsEvent(WindowsEventLogWriter.ERROR, WindowsEventLogWriter.FEED_IMPORT_ERROR, "Could not import the user desk feed");
        }

        this.session.close();

    }

    private void importUserMapping(File mapping) throws IOException, JDOMException {
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(mapping);

        Element root = document.getRootElement();
        deskTable = new Hashtable<String, Desk>();
        logaccs.info("Start Import :" + root.getAttributeValue("Fullname") + " release :" + root.getAttributeValue("InternalRelease") + " date :" + root.getAttributeValue("Date"));

        Element hri = root.getChild("HRI");

        // iterate over all USERS
        this.session.beginTransaction();
        for (Element useridElement : hri.getChildren("USERID")) {

            User user = new User();
            user.setActive(false);
            user.setUserid(useridElement.getAttributeValue("VALUE"));
            logaccs.info("Import UserID:" + user.getUserid());
            HashSet<Desk> desklist = new HashSet<>();
            for (Element ou : useridElement.getChildren("OU")) {

                // check if the OU is already there 
                String ouName = ou.getAttributeValue("VALUE");
                if (deskTable.containsKey(ouName)) {
                    desklist.add(deskTable.get(ouName));

                } else {
                    Desk desk = new Desk();
                    desk.setName(ouName);
                    desk.setActive(false);
                    this.session.save(desk);
                    deskTable.put(ouName, desk);
                    desklist.add(desk);
                }

                logaccs.debug("\t add Desk:" + ouName);
            }
            user.setDesks(desklist);
            this.session.save(user);

        }
        this.session.getTransaction().commit();
        logaccs.info("Finishing user import");

    }

    private void importOU(File mapping) throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(mapping);

        Element root = document.getRootElement();
        
        logaccs.info("Start Import :" + root.getAttributeValue("Fullname") + " release :" + root.getAttributeValue("InternalRelease") + " date :" + root.getAttributeValue("Date"));

        Element crm = root.getChild("CRM");
        //assuming the user import is alredy done
        this.session.beginTransaction();
        for (Element ouElement : crm.getChildren("OU")) {
            String ou = ouElement.getAttributeValue("VALUE");
            logaccs.info("import access list for OU :" + ou);

            Desk desk = this.deskTable.get(ou);

            if (desk != null) {
                importPermissions(ouElement.getChildren(), null, desk, 1);
            } else {
                logaccs.error("No desk Entry found for :" + ou);
            }

        }
        this.session.getTransaction().commit();

    }

    private void importPermissions(List<Element> fields, Permissions parrent, Desk desk, int level) {
        String levelMarker = "-";
        for (int i = 0; i <= level; i++) {
            levelMarker = levelMarker.concat("-");
        }
        if (fields != null) {

            //HashSet<Permission> permissionList = new HashSet<Permission>();
            for (Element field : fields) {
                Permissions permissions = new Permissions();
                permissions.setFieldName(field.getName());
                permissions.setValue(field.getAttributeValue("VALUE"));
                permissions.setParrent(parrent);
                permissions.setDesk(desk);
                permissions.setActive(false);

                this.session.save(permissions);
                
                // set implicit and if ther is  a tree structure
                if ((parrent!=null) &&( desk.getOperator().compareToIgnoreCase("AND")!=0))
                {
                    logaccs.debug("Set for Desk:"+desk.getName()+" an implecite and");
                    desk.setOperator("AND");
                    this.session.update(desk);
                }

                // just to make it nice
                logaccs.debug("\t" + levelMarker + "> name :" + field.getName() + " value:" + field.getAttributeValue("VALUE"));

                // recursiv regresion 
                importPermissions(field.getChildren(), permissions, desk, level + 1);
               // add sublist if exist

            }
        }
    }
    
    private void switchePassivToActive()
    {
        cleanup(true);
        Query updatePermissions=this.session.createQuery("UPDATE Permissions set active = true   WHERE active = false");
        Query updateDesk=this.session.createQuery("UPDATE Desk set active = true   WHERE active = false");
        Query updateUser=this.session.createQuery("UPDATE User set active = true   WHERE active = false");
        
         this.session.beginTransaction();
         updatePermissions.executeUpdate();
         updateDesk.executeUpdate();
         updateUser.executeUpdate();
         this.session.getTransaction().commit();
         logaccs.info("Switche from passive to active");
        
    }

    private void cleanup(boolean active) {
        this.session.beginTransaction();

        logaccs.info("cleanup db");

        Query delpermission = this.session.createQuery("delete from Permissions where active=:act");
        Query deluser = this.session.createQuery("delete from User where active=:act ");
        Query deldesk = this.session.createQuery("delete from Desk where active=:act ");
        //Query delgroup = this.session.createQuery("delete from UserGroup where active=:act ");
        delpermission.setBoolean("act", active);
        deluser.setBoolean("act", active);
        deldesk.setBoolean("act", active);

        delpermission.executeUpdate();
        deluser.executeUpdate();
        deldesk.executeUpdate();

        this.session.getTransaction().commit();

    }

}
