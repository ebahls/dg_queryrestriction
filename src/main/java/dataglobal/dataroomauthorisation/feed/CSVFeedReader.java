/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation.feed;

import dataglobal.dataroomauthorisation.JDBCDataRoomPluginFactory;
import dataglobal.dataroomauthorisation.model.JDBCQueryRestrictionImpl;
import dataglobal.dataroomauthorisation.model.database.Desk;
import dataglobal.dataroomauthorisation.model.database.Permissions;
import dataglobal.dataroomauthorisation.model.database.User;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author eobs
 */
@DisallowConcurrentExecution
public class CSVFeedReader implements Job {

    static Logger log = LogManager.getLogger(CSVFeedReader.class.getName());
    static Logger logaccs = LogManager.getLogger("access");
    private Session session;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        this.session = JDBCDataRoomPluginFactory.getSession();
        try {
            String path = context.getMergedJobDataMap().getString("feedImporter.in.path");

            File user = new File(path, context.getMergedJobDataMap().getString("feedImporter.in.user"));
            File desk = new File(path, context.getMergedJobDataMap().getString("feedImporter.in.permissions"));

            logaccs.info("Start CSV Import ");
            logaccs.info("Using user csv from:" + user.getAbsoluteFile());
            logaccs.info("Using desk csv from:" + desk.getAbsoluteFile());

            importData(user, desk);
        } catch (IOException ex) {
            log.error(ex);
        }

        this.session.close();

    }

    private void importData(File userFile, File permissionFile) throws FileNotFoundException, IOException {
        // clean tables
        this.session.beginTransaction();

        Query delpermission = this.session.createQuery("delete from Permissions");
        Query deluser = this.session.createQuery("delete from User");
        Query deldesk = this.session.createQuery("delete from Desk");
        
        delpermission.executeUpdate();
        deluser.executeUpdate();
        deldesk.executeUpdate();
       

        this.session.getTransaction().commit();
        logaccs.info("Cleanup Database");

        this.session.beginTransaction();
        logaccs.info("Start import phase");

        // import user desk from csv
        HashMap<String, Set<Desk>> user_desk = new HashMap<>();
        HashMap<String, Desk> desks = new HashMap<>();
        HashSet<User> userlist = new HashSet<>();

        Reader in = new FileReader(userFile);

        Iterable<CSVRecord> records = CSVFormat.EXCEL.withQuote(null).withDelimiter(';').parse(in);
        for (CSVRecord record : records) {
            String username = record.get(0);
            String deskname = record.get(1);

            Desk desk;

            if (!desks.containsKey(deskname)) {
                desk = new Desk();
                desk.setName(deskname);
                desks.put(deskname, desk);
            } else {
                desk = desks.get(deskname);
            }

            User user;

            if (!user_desk.containsKey(username)) {

                user = new User();
                user.setUserid(username);

                HashSet<Desk> desklist = new HashSet<>();
                desklist.add(desk);
                user_desk.put(username, desklist);
                userlist.add(user);

            } else {

                Set<Desk> desklist = user_desk.get(username);
                desklist.add(desk);

            }

        }

        // build final Objects
        for (User u : userlist) {
            u.setDesks(user_desk.get(u.getUserid()));
            this.session.save(u);

        }

        this.session.getTransaction().commit();
        logaccs.info("Import user and desks finished");
        this.session.beginTransaction();
        // import permissions
        in = new FileReader(permissionFile);
        long count = 0;
        records = CSVFormat.EXCEL.withQuote(null).withDelimiter(';').parse(in);
        for (CSVRecord record : records) {
            count++;
            String desk = record.get(0);
            String field = record.get(1);
            String value = record.get(2);

            Desk d = desks.get(desk);

            if (d != null) {
                Permissions permission = new Permissions();
                permission.setFieldName(field);
                permission.setValue(value);
                permission.setDesk(d);
                session.save(permission);
            }

        }

        this.session.getTransaction().commit();
        logaccs.info("Import permissions finished [" + count + "] records stored");

    }

}
