/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation.feed;

import dataglobal.dataroomauthorisation.JDBCDataRoomPluginFactory;
import dataglobal.dataroomauthorisation.model.database.UserGroup;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author eobs
 */
public class WindowsLocalGroupImporter implements Job {

    static Logger log = LogManager.getLogger("WindowsLocalGroupImporter");
    static Logger logaccs = LogManager.getLogger("access");

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        // import for no QR for groupmembers
        Session session = JDBCDataRoomPluginFactory.getSession();
        String group = context.getMergedJobDataMap().getString("groupimporter.access.group");
        logaccs.info("Windows Group import no QR for groupmembers of:" + group);

        session.getTransaction().begin();
        for (String member : this.importLocalGroupMembers(group)) {

            UserGroup usergroup = new UserGroup();
            usergroup.setActive(false);
            usergroup.setUserid(member);
              usergroup.setDynamic_ref("#NO_QR");

            logaccs.info("No query restriction for user " + member);

            session.save(usergroup);

        }

        for (String key : context.getMergedJobDataMap().getKeys()) {
            if (key.startsWith("groupimporter.access.dynamic.")) {

                //extract name
                String name = key.substring(key.lastIndexOf(".") + 1);
                log.debug("found key:" + key + " using reference name:" + name);

                for (String member : importLocalGroupMembers(context.getMergedJobDataMap().getString(key))) {

                    UserGroup usergroup = new UserGroup();
                    usergroup.setActive(false);
                    usergroup.setUserid(member);
                    usergroup.setDynamic_ref(name);

                    logaccs.info("Add member " + member+" with group reference :"+name);

                    session.save(usergroup);

                }

            }
        }

        Query delete = session.createQuery("DELETE UserGroup     WHERE active = true");
        Query update = session.createQuery("UPDATE UserGroup set active = true   WHERE active = false");

        delete.executeUpdate();
        update.executeUpdate();

        session.getTransaction().commit();

        session.close();

    }

    public ArrayList<String> importLocalGroupMembers(String group) {
        CommandLine cmdLine = new CommandLine("net");
        cmdLine.addArgument("localgroup");
        cmdLine.addArgument(group);

        log.info(cmdLine.toString());

        ArrayList<String> result = new ArrayList<String>();

        DefaultExecutor executor = new DefaultExecutor();
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
            executor.setStreamHandler(streamHandler);
            executor.execute(cmdLine);

            String lines[] = outputStream.toString().split("\\r?\\n");
            boolean startParse = false;
            for (int i = 0; i < lines.length - 1; i++) {
                if (startParse) {

                    String userID = lines[i];
                    userID = userID.replaceAll("\\\\", "@");
                    if (userID.contains("@")) {
                        userID = userID.split("@")[1];
                    }

                    result.add(userID);

                }
                if (lines[i].startsWith("----")) {
                    startParse = true;
                }

            }

        } catch (IOException ex) {
            log.error(ex);
        }

        return result;
    }

}
