/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.dataroomauthorisation.feed;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;

/**
 *
 * @author eobs
 */
public class JDBCDataRoomFeedImporter implements Job{

    static  Logger log = LogManager.getLogger(JDBCDataRoomFeedImporter.class.getName());
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        
        JobKey jobKey = jec.getJobDetail().getKey();  
     log.info("SimpleJob says: " + jobKey );  
        
    }
    
}
