/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.dataroomauthorisation.model;

import dataglobal.queryrestrictionserver.*;
import dataglobal.queryrestrictionserver.model.FieldMatching;
import dataglobal.queryrestrictionserver.model.Stamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author eobs
 */
public class BBSPluginImpl implements QueryRestrictionPlugin{

    static  Logger log = LogManager.getLogger(BBSPluginImpl.class.getName());
    static  Logger logaccs = LogManager.getLogger("access");
    private QueryRestrictionStampSetup setup;
    //private ArrayList<String> mandetoryFields;
    private BBSFeedReader bbsFeed;
    private ArrayList<String> validIDS;
//    private String userID;
    private String desk;
    private Stamp stamp;
    
    public BBSPluginImpl(BBSFeedReader bbsFeed) {
        
        this.bbsFeed=bbsFeed;
        
        log.warn("This implementation is just for testing");
    }
    
    
    
    
    @Override
    public void initStamp(long sid, String domain, String user, dataglobal.queryrestrictionserver.model.Stamp stamp) {
        //this.userID=domain+"\\"+user;
        this.desk=this.bbsFeed.getDesk().getDeskByUserID(domain+"\\"+user);
        this.stamp=stamp;
        
        log.debug("Domain:"+domain);
        log.debug("User:"+user);
        log.debug("Desk:"+desk);
        log.debug("Stamp:"+stamp.getName());
        
        for (String field:stamp.getFields())
        {
            log.debug("Avalible Stamp fields:"+field);
        }
        
        
        //this.mandetoryFields=stamp.getFields();
        
         QueryRestrictionStampSetup setup=new QueryRestrictionStampSetup();
        
        
        this.validIDS=bbsFeed.getPartnerIDs().getValidPartnerIDs(desk);
        
        
        
        this.setup=setup;
       
        
    }

  
    @Override
    public QueryRestrictionStampSetup getStampSetup(ArrayList<String> availableFields) {
        
         setup.setUsedStampFields(availableFields, this.stamp);
         return setup;
    }

    @Override
    public Set<String> getAllowedValues(String field) {
        
        
        
        String mapping=this.stamp.getFieldMapping().get(field);
        
        log.debug("Field:"+field+" is mappet to:"+mapping);
        log.warn("this implementation just knows partnerid field different field names are ignored");
        
        return new HashSet<String>(this.validIDS);
        
        
    }

    @Override
    public boolean hasQueryRestriction() {
        return true;
    }

    @Override
    public void tearDown() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getImpliciteOperator() {
      return null;
    }

    @Override
    public ArrayList<FieldMatching> getFieldMatchingDescription() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

 
    
    

     
   
   
    
}
