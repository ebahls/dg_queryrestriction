/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation.model;

import com.google.gson.Gson;
import dataglobal.queryrestrictionserver.model.FieldMatching;
import dataglobal.queryrestrictionserver.net.ResponseObject;
import dataglobal.queryrestrictionserver.security.SignatureHelper;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author eobs
 */
public class QueryRestrictionResult implements ResponseObject {
    
    byte buffer[];
    static Logger log = LogManager.getLogger(QueryRestrictionResult.class.getName());
    static Logger logaccs = LogManager.getLogger("access");
    private long sid;
    private String id = "";
    private List<FieldMatching> matchinglist;
//    private Element allowedValuesStructure;
    
    public void setID(String id) {
        if (id != null) {
            this.id = id;
        }
    }
    
    public void setSid(long sid) {
        this.sid = sid;
    }
    
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
    public void setFieldoperator(String fieldoperator) {
        this.fieldoperator = fieldoperator;
    }
    
    private boolean enabled;
    private String fieldoperator;
    
    private Hashtable<String, Set<String>> allowedValues = new Hashtable<String, Set<String>>();
    
    public byte[] getContent() {
        return this.buffer;
    }
    
    @Override
    public int getContentLengt() {
        return this.buffer.length;
    }
    
    public void setAllowedValue(String name, Set<String> list) {
        this.allowedValues.put(name, list);
    }
    
    public void setAlloawdElementsXML(Element xmlElements)
    {
        //this.allowedValuesStructure=xmlElements;
    }
    
    public void setFieldMatchingValues(List<FieldMatching> list)
    {
        this.matchinglist=list;
    }
    
    @Override
    @Deprecated
    public void build() {
        
        Gson gson = new Gson();
        String json = "{\n";
        json = json.concat("enabled:" + gson.toJson(this.enabled) + ",\n");
        json = json.concat("fieldoperator:" + gson.toJson(this.fieldoperator) + ",\n");
        
        json = json.concat("fields:" + gson.toJson(this.allowedValues.keySet()) + ",\n");
        
        json = json.concat("fieldoperator:" + gson.toJson(this.fieldoperator) + ",\n");
        
        for (String key : this.allowedValues.keySet()) {
            json = json.concat(key + ":" + gson.toJson(this.allowedValues.get(key)) + ",\n");
        }
        
        json = json.concat("}");
        
        this.buffer = json.getBytes();
        
    }
    
    @Override
    public void buildXML() {
        
        Document doc = new Document();
        Element element = new Element("result");
        doc.setRootElement(element);
        
        Element meta = new Element("meta");
        meta.addContent(new Element("enabled").setText(String.valueOf(this.enabled)));
        meta.addContent(new Element("fieldoperator").setText(this.fieldoperator));
       
        doc.getRootElement().addContent(meta);
        SignatureHelper signature = null;
        
        try {
            signature = new SignatureHelper(this.enabled, this.fieldoperator,id);
            
       
        // add field matching into the meta tag
            
            if (this.matchinglist!=null)
            {
                for (FieldMatching fm:this.matchinglist)
                {
                    Element fmelement=new Element("fieldmatch");
                    
                    fmelement.setAttribute("name", fm.getFieldname());
                    fmelement.setAttribute("pattern", fm.getPattern());
                    fmelement.setAttribute("access", Boolean.valueOf(fm.isAcces()).toString());
                    
                    meta.addContent(fmelement);
                }
            }
        
        
        for (String key : this.allowedValues.keySet()) {
            Element field = new Element("fields");
            field.setAttribute("name", key);
            
            for (String value : this.allowedValues.get(key)) {
                signature.addValue(value);
                field.addContent(new Element("l").setText(value));
            }
            
            element.addContent(field);
            
        }
        
        
        meta.addContent(new Element("signature").setText(signature.getSignature()));
        //element.setAttribute("signature",signature.getSignature());
        logaccs.info("SID:" + this.sid + " SHA-1 Hash value :[" + signature.getSHA1() + "]");
        Format format=Format.getPrettyFormat();
       
        format.setEncoding("UTF-8");
        XMLOutputter xmlOutputter = new XMLOutputter(format);
        
        this.buffer = xmlOutputter.outputString(doc).getBytes("UTF-8");
        
         } catch (NoSuchAlgorithmException ex) {
            log.error(ex);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            log.error(ex);
        }
    }
    
}
