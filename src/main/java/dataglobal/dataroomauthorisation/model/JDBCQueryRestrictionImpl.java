/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation.model;

import dataglobal.dataroomauthorisation.JDBCDataRoomPluginFactory;
import dataglobal.dataroomauthorisation.model.database.Desk;
import dataglobal.dataroomauthorisation.model.database.User;
import dataglobal.dataroomauthorisation.model.database.UserGroup;
import dataglobal.queryrestrictionserver.QueryRestrictionPlugin;
import dataglobal.queryrestrictionserver.QueryRestrictionStampSetup;
import dataglobal.queryrestrictionserver.ServerFactory;
import dataglobal.queryrestrictionserver.model.FieldMatching;
import dataglobal.queryrestrictionserver.model.Stamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author eobs
 */
public class JDBCQueryRestrictionImpl implements QueryRestrictionPlugin {

    static Logger log = LogManager.getLogger(JDBCQueryRestrictionImpl.class.getName());
    static Logger logaccs = LogManager.getLogger("access");
    private Session session;
    private Set<dataglobal.dataroomauthorisation.model.database.Desk> desks;
    private Stamp stamp;
    private QueryRestrictionStampSetup setup;
    private String userName;
    private long sid;
    private boolean grant_unknowen_user=true;
    private boolean has_query_restriction=true;
    private String impliciteOperator;
    private List<UserGroup> memberOf;

    public JDBCQueryRestrictionImpl(Session session,boolean grantUnknowenUser) {
        this.session = session;
        this.grant_unknowen_user=grantUnknowenUser;

    }

    @Override
    public void initStamp(long sid, String domain, String user, dataglobal.queryrestrictionserver.model.Stamp stamp) {

        this.stamp = stamp;
        this.sid=sid;
        this.userName=user;
        logaccs.info("Request SID:"+sid+" from USER:" + domain + "\\" + user + " with Stamp:" + stamp.getName());

        //this.session.beginTransaction();
        
         
        
        Query qrDesk =  session.createQuery("from User u where u.userid= :username");

        qrDesk.setParameter("username", user);
 
        List<User> result = qrDesk.list();

        log.debug("SID:"+sid+" Found [" + result.size() + "] matching user");
           
      
        
        Query groupMemberQR = this.session.createQuery("from UserGroup u where lower(u.userid)= :username and u.active=true  ");
        groupMemberQR.setParameter("username", user.toLowerCase());
        
        // check if user is member of #NO_QR
        
       memberOf= groupMemberQR.list();
        
        if (groupMemberQR.list().size()>0)
        {
        for (UserGroup member:memberOf)
        {
            if (member.getDynamic_ref().compareTo("#NO_QR")==0)
            {
                logaccs.info("user:"+user+" is part of the no qr group");
                this.has_query_restriction=false;
            }
        }
        
        
        }
        
        
         if ((grant_unknowen_user)&&(result.size()==0) )
        {
          
                logaccs.info("Request SID:"+sid+" user unknowen get full accsess");
                this.has_query_restriction=false;
            
        }
        
        if (this.has_query_restriction)
        {
        for (User u : result) {
            this.desks = u.getDesks();
        }
        }
        
     
        this.setup = new QueryRestrictionStampSetup();
        log.debug("init stamp finished");

    }

    @Override
    public QueryRestrictionStampSetup getStampSetup(ArrayList<String> availableFields) {

        setup.setUsedStampFields(availableFields, this.stamp);
        return setup;
    }

    @Override

    public boolean hasQueryRestriction() {
        return this.has_query_restriction;
    }

    @Override
    public Set<String> getAllowedValues(String field) {

        String fieldName = this.stamp.getFieldMapping().get(field);

        Query qrPerm = this.session.createQuery("select p.value from Permissions p where p.desk=:desk and p.fieldName=:field and p.active= true");
        HashSet<String> permitedValues = new HashSet<>();
        if (this.desks != null) {
            for (Desk d : this.desks) {
                logaccs.info("SID:"+this.sid+" user :"+this.userName + " add field:"+fieldName+" for desk:"+d.getName());
                qrPerm.setParameter("desk", d);
                qrPerm.setParameter("field", fieldName);

                if (d.getOperator().compareToIgnoreCase("AND")==0)
                        {
                            this.stamp.setOperator("AND");
                            this.impliciteOperator="AND";
                            logaccs.info("Switche to implicite AND");
                        }
                else if ((d.getOperator().compareToIgnoreCase("OR")==0))
                        {
                            this.stamp.setOperator("OR");
                            this.impliciteOperator="OR";
                            logaccs.info("Switche to implicite OR");
                        }
                List<String> values = qrPerm.list();
                permitedValues.addAll(values);
                
                

            }
        }
        else
        {
            logaccs.info("SID:"+this.sid+" No User Desk relation found for :"+this.userName);
        }
        return permitedValues;
    }
    
    @Override
    public ArrayList<FieldMatching> getFieldMatchingDescription()
    {
         ArrayList<FieldMatching> result=new ArrayList<FieldMatching>();
         
         // iterate over all field mapping descr
        for (FieldMatching fieldMatching:stamp.getFieldMatching())
        {
             String ref=fieldMatching.getMemberref();
             
             // check if the the user is in the ref list
             boolean inGroup=false;
             for (UserGroup memb:this.memberOf)
             {
                 if (memb.getDynamic_ref().compareTo(ref)==0)
                 {
                     inGroup=true;
                     logaccs.info(memb.getUserid()+" is member of the group ref ["+memb.getDynamic_ref()+"]");
                 }
             }
             
             if (inGroup)
             {
                 result.add(new FieldMatching(fieldMatching.isAcces(), fieldMatching.getPattern(), ref,fieldMatching.getFieldname()));
             }
             else
             {
                  
                 //result.add(new FieldMatching(!fieldMatching.isAcces(), fieldMatching.getPattern(), ref,fieldMatching.getFieldname()));
             }
             
        }
        
       
        
       
     return result  ;
        
        
    }

    @Override
    public void tearDown() {
        this.session.close();
        logaccs.info("SID:"+this.sid+" finished request tear down ");
    }

    @Override
    public String getImpliciteOperator() {
        return this.impliciteOperator;
    }

}
