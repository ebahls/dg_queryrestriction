/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation.model.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
 

/**
 *
 * @author eobs
 */
@Entity
@Table(name = "User_table", uniqueConstraints = {
    @UniqueConstraint(columnNames = "userid")},
        indexes = {@Index(columnList = "userid" ),@Index(columnList = "active" )})
public class User implements Serializable {

    private Long id;
    private String userid;
    private String domain;
    private boolean active;

     @Column(name = "active"  )
    public boolean isActive() {
        return active;
    }

   
    public void setActive(boolean active) {
        this.active = active;
    }
    
    private Set<Desk> desks = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL,  CascadeType.REMOVE})
    @JoinTable(name = "user_desk", joinColumns = {
        @JoinColumn(name = "userid", nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "deskid",
                        nullable = false, updatable = false)})
  
    public Set<Desk> getDesks() {
        return desks;
    }

    public void setDesks(Set<Desk> desks) {
        this.desks = desks;
    }

    @Id
    @Column(name = "userid", unique = true, nullable = false)
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Column(name = "username", unique = false, nullable = false, length = 50)
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
    
    

}
