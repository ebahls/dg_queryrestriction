/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation.model.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author eobs
 */
@Entity
@Table(name = "permission_table", indexes = {
    @Index(columnList = "parrent_id"),@Index(columnList = "fieldName" )})

public class Permissions implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parrent_id")
    private Permissions parrent;

    private String fieldName;
    private String value;

    @OneToMany(mappedBy = "id")
    private Set<Permissions> children = new HashSet<Permissions>();

    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    public Set<Permissions> getChildren() {
        return children;
    }

    public void setChildren(Set<Permissions> children) {
        this.children = children;
    }

   

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deskid", nullable = false)
    private Desk desk;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Desk getDesk() {
        return desk;
    }

    public void setDesk(Desk desk) {
        this.desk = desk;
    }

    public Permissions getParrent() {
        return parrent;
    }
    

    public void setParrent(Permissions parrent) {
        this.parrent = parrent;
    }

}
