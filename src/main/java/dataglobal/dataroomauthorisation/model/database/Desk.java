/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation.model.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author eobs
 */
@Entity
@Table(name = "desk_table" ,
        indexes = {@Index(columnList = "name" ),@Index(columnList = "active" )})
public class Desk implements Serializable {

    private long id;
    private String name;
    private Set<Permissions> permissions=new HashSet<>();
    private String operator="OR";

     @Column(name = "operator", nullable = false, length = 3, unique = false)
    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
    
@Column(name = "active"  )
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    private boolean active;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "desk",cascade = CascadeType.ALL,orphanRemoval=true)
    public Set<Permissions> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permissions> permissions) {
        this.permissions = permissions;
    }

    
     @Column(name = "name", nullable = false, length = 50, unique = false)
    public String getName() {
        return name;
    }
   
   
    public void setName(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "deskid", unique = true, nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
