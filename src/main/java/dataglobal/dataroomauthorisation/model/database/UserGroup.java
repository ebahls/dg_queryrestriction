/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.dataroomauthorisation.model.database;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author eobs
 */

@Entity
@Table(name = "usergroup_table", uniqueConstraints = {
    @UniqueConstraint(columnNames = "groupid")},
        indexes = {@Index(columnList = "groupid" ),@Index(columnList = "active" )})
public class UserGroup {

    @Id
    @Column(name = "groupid", unique = false, nullable = false)
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Column(name = "active"  )
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

   
    private String dynamic_ref;

    public String getDynamic_ref() {
        return dynamic_ref;
    }

    public void setDynamic_ref(String dynamic_ref) {
        this.dynamic_ref = dynamic_ref;
    }
    
    private Long id;
    private String userid;
    
    private boolean active;
    
    
}
