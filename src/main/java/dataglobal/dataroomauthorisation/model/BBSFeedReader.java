/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.dataroomauthorisation.model;

import dataglobal.queryrestrictionserver.ServerFactory;
import java.io.File;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author eobs
 */
public class BBSFeedReader {
    
    static  Logger log = LogManager.getLogger(BBSFeedReader.class.getName());

    public DeskMem getDesk() {
        return desk;
    }

    public PartnerIDDesk getPartnerIDs() {
        return partnerIDs;
    }
    
    private DeskMem desk=new DeskMem();
    private PartnerIDDesk partnerIDs=new PartnerIDDesk();
    
    
    public BBSFeedReader(File xmlfeed) throws JDOMException, IOException
    {
        SAXBuilder saxBuilder = new SAXBuilder(); 
        Document document = saxBuilder.build(xmlfeed);  
        
        Element root=document.getRootElement();
        
        log.info("BBS XML Reader Load:"+root.getAttributeValue("FullName")+" Release:"+root.getAttributeValue("InternalRelease"));
        
        parseUserDeskRelation(root);
        parseDeskIDRelation(root);
        
    }
    
    private void parseUserDeskRelation(Element element)
    {
        Element hri=element.getChild("HRI");

        for (Element userID:hri.getChildren("USERID"))
        {
        Element desk=userID.getChild("DESK");
        
        this.desk.addUserDeskRelation(userID.getAttributeValue("Value"), desk.getAttributeValue("Value"));
        }
    }
    
    private void parseDeskIDRelation(Element element)
    {
        Element crm=element.getChild("CRM");
        
        for (Element e:crm.getChildren("DESK"))
        {
         
         
            for (Element id:e.getChildren("PARTNERID"))
            {
                this.partnerIDs.addPartnerID(e.getAttributeValue("Value"), id.getAttributeValue("Value"));
            }
        
        
         
        }
    }
    
}
