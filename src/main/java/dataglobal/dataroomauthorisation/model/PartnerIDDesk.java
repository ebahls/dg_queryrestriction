/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.dataroomauthorisation.model;

import dataglobal.dataroomauthorisation.DataRoomAutorisationPluginFactory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author eobs
 */
public class PartnerIDDesk {
    
    
    private Hashtable<String,ArrayList<String>> PartnerIDTable = new Hashtable<String,ArrayList<String>>();
    
    
    static  Logger log = LogManager.getLogger(PartnerIDDesk.class.getName());
    
    public void addPartnerID(String desk,String id)
    {
        ArrayList<String> partnerIDs;
        if (this.PartnerIDTable.containsKey(desk))
        {
            
            partnerIDs=this.PartnerIDTable.get(desk);
            
        }
        else
        {
            log.debug("Create new Desk entry for:"+desk);
             partnerIDs=new ArrayList<String>();
            this.PartnerIDTable.put(desk, partnerIDs);
            
            
             
        }
        
        log.debug("Add new valid ID to :"+desk+"  ID:"+id);
        partnerIDs.add(id);
        
        
        
    }
    
    public ArrayList<String> getValidPartnerIDs(String desk)
    {
        return this.PartnerIDTable.get(desk);
    }
    
}
