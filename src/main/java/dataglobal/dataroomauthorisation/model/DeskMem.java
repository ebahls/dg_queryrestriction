/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.dataroomauthorisation.model;

import java.util.Hashtable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author eobs
 */
public class DeskMem {
    
    private Hashtable<String,String> userDeskRelation=new Hashtable<String, String>();
    static  Logger log = LogManager.getLogger(DeskMem.class.getName());
    
    public void addUserDeskRelation(String userID,String desk)
    {
        log.debug("Add User Desk rel User ID:"+userID+" to:"+desk);
        this.userDeskRelation.put(userID, desk);
    }
    
    public String getDeskByUserID(String userid)
    {
        return userDeskRelation.get(userid);
    }
    
}
