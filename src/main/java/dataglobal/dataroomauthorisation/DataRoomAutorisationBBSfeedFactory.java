/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.dataroomauthorisation;

import dataglobal.dataroomauthorisation.model.BBSFeedReader;
import dataglobal.dataroomauthorisation.model.BBSPluginImpl;
import dataglobal.queryrestrictionserver.QueryRestrictionPluginFactory;
import dataglobal.queryrestrictionserver.QueryRestrictionPlugin;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.JDOMException;

/**
 *
 * @author eobs
 */
public class DataRoomAutorisationBBSfeedFactory implements QueryRestrictionPluginFactory {

    static  Logger log = LogManager.getLogger(DataRoomAutorisationBBSfeedFactory.class.getName());
    private BBSFeedReader bbsFeed;
    
    
    public DataRoomAutorisationBBSfeedFactory() {
        
    }
    
     

  
    @Override
    public QueryRestrictionPlugin getPlugin() {
        return new BBSPluginImpl(bbsFeed);
    }

    @Override
    public void setup(HashMap<String, String> config) {
        
        log.debug("Load Config from :"+config.get("path"));
        
        try {
            bbsFeed=new BBSFeedReader(new File(config.get("path")));
        } catch (JDOMException ex) {
           log.trace(ex);
        } catch (IOException ex) {
            log.trace(ex);
        }
        finally
        {
        
        log.debug("Setup is done");
        }
    }
    
}
