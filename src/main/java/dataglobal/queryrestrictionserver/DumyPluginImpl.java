/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.queryrestrictionserver;

 
import dataglobal.queryrestrictionserver.model.FieldMatching;
import dataglobal.queryrestrictionserver.model.Stamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author eobs
 */
public class DumyPluginImpl implements QueryRestrictionPlugin{

    static  Logger log = LogManager.getLogger(QueryRestrictionPlugin.class.getName());
    static  Logger logaccs = LogManager.getLogger("access");
    private QueryRestrictionStampSetup setup;
//    private ArrayList<String> mandetoryFields;
    private Stamp stamp;
    @Override
    public void initStamp(long sid, String domain, String user, dataglobal.queryrestrictionserver.model.Stamp stamp) {
        log.debug("Domain:"+domain);
        log.debug("User:"+user);
        log.debug("Stamp:"+stamp);
        
        this.stamp=stamp;
        
        
        for (String field:stamp.getFields())
        {
            log.debug("Field:"+field);
        }
        
        //this.mandetoryFields=stamp.getFields();
        
        QueryRestrictionStampSetup setup=new QueryRestrictionStampSetup();
        
        /**
        if (stamp.compareToIgnoreCase("Test")==0)
        {
            log.debug("QR  Supportet");
            
            ArrayList<String> mf=new ArrayList<String>();
            mf.add("ID1");
            
            setup.setUsedStampFields(fields, mf);
            
            log.debug("Status:"+setup.getStatus());
            for (int i:setup.getUsedStampFields())
            {
                log.debug("Index:"+i);
            }
        }
        
         if (stamp.compareToIgnoreCase("UBSTest")==0)
        {
            log.debug("QR  Supportet");
            
            ArrayList<String> mf=new ArrayList<String>();
            mf.add("PARTNERID");
            
            setup.setUsedStampFields(fields, mf);
            
            log.debug("Status:"+setup.getStatus());
            for (int i:setup.getUsedStampFields())
            {
                log.debug("Index:"+i);
            }
        }
        
         
        else if ((stamp.compareToIgnoreCase("StexNet")==0)||(stamp.compareToIgnoreCase("UBSTest")==0))
        {
            logaccs.info("Query Restriction Disbale for stamp :"+stamp+" sid:"+sid);
            setup.disableQueryRestriction();
        }
         */
        this.setup=setup;
        
        
    }

   
    @Override
    public QueryRestrictionStampSetup getStampSetup(ArrayList<String> availableFields) {
        
         setup.setUsedStampFields(availableFields, this.stamp);
         return setup;
    }

    @Override
    public Set<String> getAllowedValues(String field) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean hasQueryRestriction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void tearDown() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getImpliciteOperator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<FieldMatching> getFieldMatchingDescription() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



     
   
   
    
}
