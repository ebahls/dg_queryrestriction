/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver;

import static dataglobal.queryrestrictionserver.QueryRestrictionDisabledImpl.logaccs;
import dataglobal.queryrestrictionserver.model.FieldMatching;
import dataglobal.queryrestrictionserver.model.Stamp;
import java.util.ArrayList;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author eobs
 */
public class QueryRestrictionNoValueImpl implements QueryRestrictionPlugin{
    
     static  Logger logaccs = LogManager.getLogger("access");
     private  QueryRestrictionStampSetup setup;

    @Override
    public void initStamp(long sid, String domain, String user, Stamp stamp) {
        logaccs.info("SID:"+ sid+" domain:"+domain+" user:"+user+" Stamp:"+stamp.getName()+" unknowen stamp no values granted");
         QueryRestrictionStampSetup setup=new QueryRestrictionStampSetup();
         setup.stampNotSupportet();
         this.setup=setup;
    }

    @Override
    public QueryRestrictionStampSetup getStampSetup(ArrayList<String> availableFields) {
       return this.setup;
    }

    @Override
    public Set<String> getAllowedValues(String field) {
        return null;
    }

    @Override
    public boolean hasQueryRestriction() {
       return true;
    }

    @Override
    public void tearDown() {
         
    }

    @Override
    public String getImpliciteOperator() {
       return null;
    }

    @Override
    public ArrayList<FieldMatching> getFieldMatchingDescription() {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }
    
}
