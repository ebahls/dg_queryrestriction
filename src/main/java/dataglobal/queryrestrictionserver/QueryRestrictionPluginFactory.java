/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.queryrestrictionserver;

import java.util.HashMap;

/**
 *
 * @author eobs
 */
public interface QueryRestrictionPluginFactory {

       QueryRestrictionPlugin getPlugin();
      void setup(HashMap<String, String> config);
    
}
