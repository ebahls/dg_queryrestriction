/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver;

import dataglobal.queryrestrictionserver.model.Stamp;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import org.quartz.CronTrigger;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

/**
 *
 * @author eobs
 */
public class ServerFactory {

    static Logger log = LogManager.getLogger(ServerFactory.class.getName());

    private HashMap<String, QueryRestrictionPluginFactory> pluginFactory = new HashMap<String, QueryRestrictionPluginFactory>();
    private HashMap<String, Stamp> stampconfiguration = new HashMap<String, Stamp>();
    private HashMap<String, String> serverconfiguration = new HashMap<String, String>();

    private boolean unknowenStampeAllowed = false;
    private static final String jobGroup="QRJobGroub";
    private static ServerFactory conf;
    private boolean protocollObfuscating=false;
     
    
    private SchedulerFactory sf;

    public ServerFactory(File configfile) throws JDOMException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
         conf=this;
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(configfile);
        // parse the config file
        // avalible plugins and Server config

        Element rootNode = document.getRootElement();
        if (rootNode.getName().equalsIgnoreCase("configuration")) {
            processingServerConfiguration(rootNode.getChild("server"));

            for (Element plugins : rootNode.getChildren("pluginfactory")) {
                processPluginFactoryConfig(plugins);
            }
            //processPluginFactoryConfig(rootNode.getChild("pluginfactory"));

            this.unknowenStampeAllowed = Boolean.parseBoolean(rootNode.getChild("queryrestriction").getAttribute("crant_unknowen").getValue());

            if (this.unknowenStampeAllowed) {
                log.info("Crant accsess for unknowen stamps");
            } else {

                log.info("denied accsess for unknowen stamps");

            }

            processStamp(rootNode.getChild("queryrestriction"));

        } else {
            log.error("wrong config file");
        }
      
    }

    public static ServerFactory getInstance()
    {
        return conf;
    }
    
    private void processingServerConfiguration(Element server) {
        if (server != null) {
            List<Element> properties = server.getChildren("property");
            for (Element property : properties) {
                this.serverconfiguration.put(property.getAttributeValue("name"), property.getAttributeValue("value"));

                log.info("Add server config property :" + property.getAttributeValue("name") + "=" + property.getAttributeValue("value"));
                
            }
            
            if ((this.serverconfiguration.containsKey("protocoll.obfuscating"))&&(this.serverconfiguration.get("protocoll.obfuscating").compareToIgnoreCase("true")==0))
            {
               
                    log.info("Enable protocoll obfuscating");
                    this.protocollObfuscating=true;
                
            }
            
        }

    }

    private void processStamp(Element queryrestriction) {
        
        for (Element stamp : queryrestriction.getChildren("stamp")) {
            String stampname = stamp.getAttributeValue("name");
            String plugin = stamp.getAttributeValue("plugin");
            String operator = stamp.getAttributeValue("operator");
            String pattern = stamp.getAttributeValue("pattern");

            if (this.pluginFactory.containsKey(plugin)) {
                log.info("Register stamp:" + stampname + " with plugin:" + plugin);
                // add mandatory fields

                ArrayList<String> fields = new ArrayList<>();

                HashMap<String, String> fieldMapping = new HashMap<>();

                // setup stamp field description
                for (Element f : stamp.getChildren("field")) {
                    String field = f.getAttributeValue("name");
                    String mapping = f.getAttributeValue("mapping");
                    log.debug("Stamp:" + stampname + " has mandatory field :" + field + " mapped to :" + mapping);

                    fieldMapping.put(field, mapping);

                    fields.add(field);

                }
                Stamp st = new Stamp();
                // setup stamp field matching description
                for (Element f : stamp.getChildren("fieldmatch")) {
                    
                    String field = f.getAttributeValue("name");
                    String patter = f.getAttributeValue("pattern");
                    String memberref = f.getAttributeValue("memberof");
                    boolean access=Boolean.valueOf(f.getAttributeValue("access"));
                    
                    log.debug("Stamp:" + stampname + " has field matching values: if (" + field + " matched with " + patter+") ="+access);

                    

                    st.addFieldMatching(field, patter, access, memberref);

                }
                
                
                
                st.setFields(fields);
                st.setName(stampname);
                st.setPlugin(plugin);
                st.setOperator(operator);
                st.setFieldMapping(fieldMapping);
                if (pattern!=null)
                {
                    st.setPattern(pattern);
                    log.info("using pattern matching for stamp pattern:"+pattern);
                }

                this.stampconfiguration.put(stampname, st);

            } else {
                log.error("register stamp:" + stampname + "not posible plugin:" + plugin + " not found");
            }

        }
    }
    

    private void processPluginFactoryConfig(Element pluginfactory) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        String name = pluginfactory.getAttributeValue("name");
        String className = pluginfactory.getAttributeValue("class");
        log.debug("PluginFactory :" + name + " Class:" + className);
        HashMap<String, String> properties = new HashMap<String, String>();
        for (Element propeElement : pluginfactory.getChildren("property")) {
            String pName = propeElement.getAttribute("name").getValue();
            String pValue = propeElement.getAttribute("value").getValue();
            properties.put(pName, pValue);
            log.debug("Plugin :" + name + " add Property :" + pName + " Value :" + pValue);
        }
        log.debug("Try to instanciate the Factory");
        Class cl = Class.forName(className);
        QueryRestrictionPluginFactory factory = (QueryRestrictionPluginFactory) cl.newInstance();
        factory.setup(properties);
        this.pluginFactory.put(name, factory);

    }

    public  SchedulerFactory getScedulerFactory()
    {
        if (this.sf==null) 
        {
            try {
        // Quartz config
        Properties props = new Properties();
        props.setProperty("org.quartz.scheduler.skipUpdateCheck", "true");

        props.setProperty("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
        props.setProperty("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
        props.setProperty("org.quartz.threadPool.threadCount", "4");
        
        sf = new StdSchedulerFactory(props);
       
        
        

        } catch (SchedulerException ex) {
            log.debug(ex);

       
        }
        }
            
            return sf;
    }
    
    public void fireJobNow(String identifier)
    {
        try {
            Scheduler sched= getScedulerFactory().getScheduler();
            boolean foundJob=false;
            for (JobKey jobKey:sched.getJobKeys(GroupMatcher.anyJobGroup()))
            {
                if (jobKey.getName().compareTo(identifier)==0) 
                {
                    sched.triggerJob(jobKey);
                    foundJob=true;
                }
            }
            
            if (!foundJob)
            {
                log.error("Job:"+identifier+" not found");
            }
            
        } catch (SchedulerException ex) {
            log.error("Cant Start Job:"+identifier);
        }
       
    }
    
    public void addNewJob(String cronexpr,String className,String identifier,Map<String,String> config)
    {
        try
        {
        SchedulerFactory sFactory = getScedulerFactory();
       

        log.info("Add new Job:"+identifier+" with cronn :["+cronexpr+"]");
        
         Scheduler sched  =  sFactory.getScheduler();

         Class cl = Class.forName(className);

            JobDetail job = newJob(cl).withIdentity(identifier, jobGroup).build();
            // put the Job config to the context
            
            if (config!=null) job.getJobDataMap().putAll(config);

            CronTrigger trigger = newTrigger()
                    .withIdentity(identifier, jobGroup)
                    .withSchedule(cronSchedule(cronexpr))
                    .build();
            sched.scheduleJob(job, trigger);
            log.debug("Add Job finished");

            sched.start();

        } catch (SchedulerException ex) {
            log.debug(ex);

        } catch (ClassNotFoundException ex) {
            log.error(ex);
        }
    }
    
     
    public synchronized QueryRestrictionPlugin getPlugin(long sid, String domain, String user, String stamp) {
        QueryRestrictionPlugin plugin = null;

        if (this.stampconfiguration.containsKey(stamp)) {
            plugin = this.pluginFactory.get(this.stampconfiguration.get(stamp).getPlugin()).getPlugin();
            plugin.initStamp(sid, domain, user, this.stampconfiguration.get(stamp));
        } else {
            
            // if no stamp present try pattern matching
            
            for (String key:this.stampconfiguration.keySet())
            {
                Stamp st=this.stampconfiguration.get(key);
                
                if (st.isPatternMatching(stamp))
                {
                    log.info("Found Stamp by pattern matching Stamp:"+stamp+" using stamp config:"+key);
                    plugin = this.pluginFactory.get(this.stampconfiguration.get(key).getPlugin()).getPlugin();
                    plugin.initStamp(sid, domain, user, this.stampconfiguration.get(key));
                    return plugin;
                }
                
            }
            
            Stamp dummyStamp=new Stamp();
            dummyStamp.setName(stamp);
            
            if (this.unknowenStampeAllowed) {

                plugin = new QueryRestrictionDisabledImpl();
                plugin.initStamp(sid, domain, user, dummyStamp);

            } else {
                plugin = new QueryRestrictionNoValueImpl();
                plugin.initStamp(sid, domain, user, dummyStamp);

            }
        }

        return plugin;

    }

    public String getServerConfigProperty(String name) {
        return this.serverconfiguration.get(name);
    }
    
    public  synchronized boolean hasProtocollObfuscating()
    {
        return this.protocollObfuscating;
    }

}
