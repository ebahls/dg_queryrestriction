/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver;

import dataglobal.dataroomauthorisation.model.QueryRestrictionResult;
import dataglobal.queryrestrictionserver.net.HttpGetRequestHandler;
import dataglobal.queryrestrictionserver.net.HttpRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author eobs
 */
public class QueryRestrictionHttpRequest implements HttpRequest {

    static Logger log = LogManager.getLogger(QueryRestrictionHttpRequest.class.getName());
    private ServerFactory config;

    private static long SID = 0;

    public QueryRestrictionHttpRequest(ServerFactory conf) {
        this.config = conf;

    }

    @Override
    public boolean accept(HashMap<String, String> requestMap, HttpGetRequestHandler handler) throws IOException {
        log.debug("Request");
        SID++;

        return true;
    }

    @Override
    public void response(HashMap<String, String> requestMap, HttpGetRequestHandler handler) throws IOException {

        QueryRestrictionPlugin plugin = this.config.getPlugin(SID, requestMap.get("domain"), requestMap.get("user"), requestMap.get("stamp"));
        
        long start=System.currentTimeMillis();

        ArrayList<String> fields = new ArrayList<>();

        String fieldString = requestMap.get("fields");
        if (fieldString.contains(",")) 
        {
            String unparsedFields[] = fieldString.split(",");
            for (String field : unparsedFields) 
            {
                log.debug("Request stamp setup add field:"+field);
                fields.add(field);
            }
        } 
        else {
              fields.add(fieldString);
        }
        
        
         
        QueryRestrictionStampSetup setup = plugin.getStampSetup(fields);
        QueryRestrictionResult result = new QueryRestrictionResult();
 
        result.setEnabled(plugin.hasQueryRestriction());
        result.setFieldoperator(setup.getOpperator());
        result.setSid(SID);
        result.setID(requestMap.get("id"));

        
        for (String f:setup.getStampFields())
        {
            Set<String> values=plugin.getAllowedValues(f);
            
            if (values!=null) result.setAllowedValue(f,values );
        }
        
        // tear down plugin
        
        plugin.tearDown();
 
        handler.response(result);
        long remain=System.currentTimeMillis()-start;
        log.debug("Request with SID:"+SID+" took "+remain+"ms");
        ServerStatistics.countRequest(remain,result.getContentLengt());

    }

}
