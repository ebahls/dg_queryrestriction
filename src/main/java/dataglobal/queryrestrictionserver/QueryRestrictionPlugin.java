/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver;

import dataglobal.queryrestrictionserver.model.FieldMatching;
import java.util.ArrayList;
import java.util.Set;

/**
 * This Interface for a QR Plugin
 * @author eobs
 */
public interface QueryRestrictionPlugin {

    void initStamp(long sid, String domain, String user, dataglobal.queryrestrictionserver.model.Stamp stamp);

    QueryRestrictionStampSetup getStampSetup(ArrayList<String> availableFields);

    Set<String> getAllowedValues(String field);

    boolean hasQueryRestriction();

    String getImpliciteOperator();
    
    ArrayList<FieldMatching> getFieldMatchingDescription();

    void tearDown();

}
