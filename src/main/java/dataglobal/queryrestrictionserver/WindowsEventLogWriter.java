/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author eobs
 */
public class WindowsEventLogWriter implements Job{

    static Logger log = LogManager.getLogger("WindowsEventLogWriter");
    
    public static final int KEEP_ALIVE=1; 
    public static final int SERVICE_STOPED=1;        
    public static final int FEED_IMPORT_OK=20;
    public static final int FEED_IMPORT_ERROR=21;
    
    public static final String ERROR="ERROR";
    public static final String SUCCESS="SUCCESS";
    public static final String WARNING="WARNING";
    public static final String INFORMATION="INFORMATION";
    
    static final String SOURCE="QueryRestriction";
    
    
    static long startTime=0;
    
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
       
        if (startTime==0)
        {
            startTime=System.currentTimeMillis();
        }
        else
        {
        long uptime=System.currentTimeMillis()-startTime;
        
        //transforme to redabil string
        
        long days = TimeUnit.MILLISECONDS.toDays(uptime);
        uptime -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(uptime);
        uptime -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(uptime);
        uptime -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(uptime);
        
        String uptimeString=days+" days "+hours+" hours "+minutes+" min,"+seconds+" sec";
        
        log.info("Keep alive Server Uptime:"+uptimeString);
        log.debug("Fire keep alive Windows Event");
        fireWindowsEvent(INFORMATION, KEEP_ALIVE, "Application is alive Uptime:"+uptimeString);
        }
    }
    
    public static synchronized void fireWindowsEvent(String level,int id,String msg)
    {
      
         CommandLine cmdLine = new CommandLine("eventcreate");
      
         cmdLine.addArgument("/L",false);
         cmdLine.addArgument("APPLICATION",false );
         cmdLine.addArgument("/T",false );
         cmdLine.addArgument(level,false );
         cmdLine.addArgument("/SO"  ,false);
         cmdLine.addArgument(SOURCE,false );
         cmdLine.addArgument("/ID",false);
         cmdLine.addArgument(String.valueOf(id),false );
         cmdLine.addArgument("/D" ,false);
         cmdLine.addArgument( msg,true );
         
         log.debug("CMD:"+cmdLine.toString());
        DefaultExecutor executor = new DefaultExecutor();
        try {
            
            int result=executor.execute(cmdLine);
            log.debug("Fire Windows event msg result:"+result);
        } catch (IOException ex) {
            log.error("Could not fire Windows event message ");
            log.error(ex);
        }

         
         
    }
   
}
