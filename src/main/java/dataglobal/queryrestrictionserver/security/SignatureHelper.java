/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.openssl.PEMWriter;
 

/**
 *
 * @author eobs
 * 
 * This Helper Class is sign the XML file
 * 1. Add Enable or Disable as Byte value 0=Disable 1=Enable
 * 2. Add Operator as ByteValue OR=0 AND=1
 * 3. Add all Values as String
 * 4. compute the sha-1 as Base64
 * 5. encrypt the hash as Base64
 * 
 */
public class SignatureHelper {
    
     
     static  Logger logaccs = LogManager.getLogger("access");
     static Logger log = LogManager.getLogger(SignatureHelper.class.getName());
     private MessageDigest md;
     byte[] sha1hash = new byte[40]; 
     
     private static Key pubKey;
     private static Key privKey;
     private static final String publicKeyName="public.key";
     private static final String privateKeyName="private.key";
     private boolean sha1Don=false;
    
    public SignatureHelper(Boolean qrEnabled,String fieldoperator,String id) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        
     byte enabledisable=0;
     byte operator=0;
     if (qrEnabled) enabledisable=1;
     
     if (fieldoperator.equalsIgnoreCase("AND")) operator=1;
        
     md = MessageDigest.getInstance("SHA-1");
     md.reset();
     md.update(id.getBytes());
     md.update(enabledisable);
     md.update(operator);
     
     
    }
    
    public void addValue(String value) throws UnsupportedEncodingException
    {
      
      md.update(value.getBytes("UTF-8"));
    }
    
    /**
     * Return the SHA-1 Hashcode 
     * @return String encodet Base64
     */
    public String getSHA1()
    {
        if (!sha1Don) this.sha1hash=md.digest();
         
         sha1Don=true;
         Base64.Encoder encoder = Base64.getEncoder();
         if (this.sha1hash!=null) return  encoder.encodeToString(this.sha1hash);
         else return ""; 
        
         
        
    }
    
    /**
     * Encrypt the SHA-1 Hash with the given Public Key 
     * @return String encodet Base64
     */
    public String getSignature()
    {
        byte buffer[]=null;
         try {
             Cipher cipher = Cipher.getInstance("RSA");
             
              // decrypt the text using the private key
      cipher.init(Cipher.ENCRYPT_MODE, pubKey);
      getSHA1();
      buffer = cipher.doFinal(this.sha1hash);
             
         } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
             log.trace(ex);
         }
         Base64.Encoder encoder = Base64.getEncoder();
       
       
         if (buffer!=null) return  encoder.encodeToString(buffer);
         else return "";
    }
    
    
    public static void initSecurity(File keyPath) throws NoSuchAlgorithmException, NoSuchProviderException, FileNotFoundException, IOException, InvalidKeySpecException
    {
        logaccs.info("Check for private public key in:"+keyPath.getAbsolutePath());
        
        File privateKeyFile=new File (keyPath,privateKeyName);
        File publicKeyFile=new File (keyPath,publicKeyName);
        
        
        
        if (publicKeyFile.canRead()&&privateKeyFile.canRead())
        {
            logaccs.info("Found public and private key files");
            
            // laod private key
            KeyFactory kf=KeyFactory.getInstance("RSA");
            
            FileInputStream in=new FileInputStream(privateKeyFile);
            // bigger is better 
            byte buffer[]=new byte[1024*20];
            int length;
            String key;
            BigInteger keyNumber;
            length=in.read(buffer);
            
            key=new String(buffer, 0, length);
            keyNumber=new BigInteger(key,16);
            
            PKCS8EncodedKeySpec spec=new PKCS8EncodedKeySpec(keyNumber.toByteArray());
            privKey=kf.generatePrivate(spec);
            //BigInteger privk=new BigInteger(privKey.getEncoded());
            logaccs.info("Loaded Private key OK");
             
            in=new FileInputStream(publicKeyFile);
            length=in.read(buffer);
            key=new String(buffer, 0, length);
            keyNumber=new BigInteger(key,16);
            
            X509EncodedKeySpec xspec =  new X509EncodedKeySpec(keyNumber.toByteArray());
            pubKey=kf.generatePublic(xspec);
            //BigInteger pubk=new BigInteger(pubKey.getEncoded());
            logaccs.info("Loaded Public key OK");
             
        }
        else
        {
            logaccs.info("Couln't find public and private keyfiel start to generate a new pair");
             
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
             
            KeyPair pair = generator.generateKeyPair();
             
            
            pubKey = pair.getPublic();
            privKey = pair.getPrivate();
            
            PEMWriter privatepemWriter = new PEMWriter(new FileWriter(new File(keyPath,  "private.pem")));
            privatepemWriter.writeObject(privKey);
            privatepemWriter.close();
            
            BigInteger pubk=new BigInteger(pubKey.getEncoded());
            BigInteger privk=new BigInteger(privKey.getEncoded());
            
            logaccs.info("Private key OK");
        
            PrintStream prOut=new PrintStream(privateKeyFile);
            prOut.print(privk.toString(16));
            logaccs.info("Stored in :"+privateKeyFile.getAbsolutePath());
                     
            prOut.close();
            
            
  
            
            logaccs.info("Public key OK");
            PrintStream puOut=new PrintStream(publicKeyFile);
            puOut.print(pubk.toString(16));
            logaccs.info("Stored in :"+publicKeyFile.getAbsolutePath());
            puOut.close();
            
        }
       
        
    }
    
    
}
