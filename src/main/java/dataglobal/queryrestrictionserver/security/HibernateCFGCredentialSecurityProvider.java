/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver.security;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.DOMOutputter;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author eobs
 */
public class HibernateCFGCredentialSecurityProvider {
    
    static Logger log = LogManager.getLogger(HibernateCFGCredentialSecurityProvider.class.getName());
    
    private final Document document;
    private String user;
    private String password;
    private String encryptedID;
    private String encryptedPW;
    private final Cipher cipher;
    private final SecretKeySpec key_pw ;
    private final SecretKeySpec key_id;
    
    private final static String encr_user="credential.store.id";
    private final static String encr_password="credential.store.pw";
    
    
    
    

    public HibernateCFGCredentialSecurityProvider(File hibernateConfigFile) throws JDOMException, IOException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        
         
         log.info("Init Hibernate config encryption");
         
                 
         key_pw = new SecretKeySpec("ömapwqed fweqeßä".getBytes(), "AES");
         key_id = new SecretKeySpec("21ß3kdsad_0ß?+ä#".getBytes(), "AES");
         
         cipher = Cipher.getInstance("AES");
         
         log.info("Checking Hibernate config encryption");
         
         SAXBuilder saxBuilder = new SAXBuilder();
         
         this.document = saxBuilder.build(hibernateConfigFile);
         
         Element sessionFactory=this.document.getRootElement().getChild("session-factory");
         
         if (isEncryptionEnabled(sessionFactory))
         {
             if (!isEncrypted(sessionFactory))
            
             {
                 log.info("Encrypt unencryptet configuration");
                 encryptValue(sessionFactory);
                 // overwrite the original file;
              XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
                 FileOutputStream out=new FileOutputStream(hibernateConfigFile);
              xmlOutputter.output(document, out);
              log.info("Recreate file");
              
                 
                 
             }
             log.info("Decrypt configuration");
             decryptValue(sessionFactory);
         }
         
         
         
         
    }
    
    
    public org.w3c.dom.Document getDocument() throws JDOMException
    {
        
        DOMOutputter output=new DOMOutputter();
        return output.output(this.document);
       
    }
    
    private String decryptB64String(String encryptetString, SecretKeySpec key) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        
        
        Base64.Decoder decoder =Base64.getDecoder();
        
        byte buffer[]=decoder.decode(encryptetString);
        
         this.cipher.init(Cipher.DECRYPT_MODE,  key);
         
         byte decr[]=this.cipher.doFinal(buffer);
         
         return new String(decr);
        
         
    }
    
       private String encryptB64String(String plainString,  SecretKeySpec key) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
         
        Base64.Encoder encoder =Base64.getEncoder();
        
        byte buffer[]=plainString.getBytes();
               
        
         this.cipher.init(Cipher.ENCRYPT_MODE,  key);
         
         byte encr[]=this.cipher.doFinal(buffer);
         
         
         return encoder.encodeToString(encr);
        
         
    }
    
    private boolean isEncryptionEnabled(Element sessionFactory)
    {
        for (Element property:sessionFactory.getChildren("property"))
        {
            if (property.getAttributeValue("name").compareToIgnoreCase("credential.encryption")==0)
            {
                if (property.getValue().compareToIgnoreCase("TRUE")==0)
                {
                log.info("Using Credential encryption");
                return true;
                }
                else
                {
                log.info("No Credential encryption required");
                return false;
                }
            }
        }
        log.info("No Credential encryption required");
                
        return false;
    }
    
    private boolean isEncrypted(Element sessionFactory)
    {
        int count=0;
          for (Element property:sessionFactory.getChildren("property"))
        {
            if (property.getAttributeValue("name").compareToIgnoreCase(encr_user)==0)
            {
               count++;
               this.encryptedID=property.getValue();
            }
            if (property.getAttributeValue("name").compareToIgnoreCase(encr_password)==0)
            {
                this.encryptedPW=property.getValue();
                
               count++;
            }
        }
          
         if (count<2)
         {
        log.info("Not encryptet yet");
        return false;
         }
         
         else return true;
    }
    
    /**
     * Encrypte the values and replace the property entries.
     * @param element
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException 
     */
    private void encryptValue(Element element) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
          for (Element property:element.getChildren("property"))
        {
            if (property.getAttributeValue("name").compareToIgnoreCase("hibernate.connection.username")==0)
            {
              
               this.user=property.getValue();
               this.encryptedID=this.encryptB64String(this.user,this.key_id);
               
               property.setText("***********");
               
               
               
            }
            if (property.getAttributeValue("name").compareToIgnoreCase("hibernate.connection.password")==0)
            {
                this.password=property.getValue();
                this.encryptedPW=this.encryptB64String(this.password,this.key_pw);
                property.setText("***********");
            }
        }
          
          // set the new encryption properties;
          
           Element idProp=new Element("property");
           
           idProp.setAttribute("name", encr_user);
           idProp.setText(this.encryptedID);
           
           Element pwProp=new Element("property");
           
           pwProp.setAttribute("name", encr_password);
           pwProp.setText(this.encryptedPW);
           
           element.addContent(pwProp);
           element.addContent(idProp);
          
          
          
           
    }
    
    private void decryptValue(Element element) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        
        
         for (Element property:element.getChildren("property"))
        {
            if (property.getAttributeValue("name").compareToIgnoreCase(encr_user)==0)
            {
              
                
               this.user=this.decryptB64String(property.getValue(),this.key_id);
               
               
            }
            if (property.getAttributeValue("name").compareToIgnoreCase(encr_password)==0)
            {
                this.password=this.decryptB64String(property.getValue(),this.key_pw);
                 
            }
        }
         
         // substitute the XML
         
            for (Element property:element.getChildren("property"))
        {
            if (property.getAttributeValue("name").compareToIgnoreCase("hibernate.connection.username")==0)
            {
              
                
               property.setText(this.user);
               
               
            }
            if (property.getAttributeValue("name").compareToIgnoreCase("hibernate.connection.password")==0)
            {
                property.setText(this.password);
            }
        }
        
        
    }
    
    
    
}
