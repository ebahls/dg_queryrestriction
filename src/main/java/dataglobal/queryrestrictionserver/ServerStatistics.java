/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver;

import java.util.Formatter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This Class calculates and logs some statistics based on one oure
 * @author eobs
 */
public class ServerStatistics implements Job{

    static Logger log = LogManager.getLogger(ServerStatistics.class.getName());
    
    private static long requestTime=0;
    private static long requests=0;
    private static long mem=0;
    private static long contentSize=0;
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        
        if (context.getPreviousFireTime()!=null)
        {
            double duration=System.currentTimeMillis()-context.getPreviousFireTime().getTime();
        if (requests>0)
        {
            // requests per minute
            double avgReqTime=(requestTime /requests);
            double avgUsedMem=mem/requests;
            double avgContentSize=contentSize/requests;
            
            String durationString=String.format("%1$,.2f min",  duration/(60*1000));
            String avgReqTimeString=String.format("%1$,.2f ms",  avgReqTime);
            String memString=String.format("%1$,.2f MB",  avgUsedMem/(1024*1024));
            String avgContentSizeString=String.format("%1$,.3f MB",   avgContentSize/(1024*1024));
            String toatalContentSizeString=String.format("%1$,.3f MB",  (double) contentSize/(1024*1024));
            
            log.info("Requests  ["+requests+"] in "+ durationString  );
            log.info("Avg req. Time  per Request : "+avgReqTimeString);
            log.info("Avg content size : "+avgContentSizeString);
            log.info("Total content size : "+toatalContentSizeString);
            
            log.info("Avg used Memory : "+memString+" in "+  durationString );
        }
        else
        {
            double  currentUsedMem= (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
            String memString=String.format("%1$,.2f MB", currentUsedMem  /(1024*1024));
            log.info("Current used memory : "+memString);
        }
       
 
        
        }
         
        requests=0;
        requestTime=0;
        mem=0;
   contentSize=0;
        
        
        System.gc();
         
    }
    
    /**
     * count a finished request for statistics
     * @param time 
     */
    public synchronized static void countRequest(long time,long size)
    {
        requestTime+=time;
        requests++;
        mem+= (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
        contentSize+=size;
    }
    
}
