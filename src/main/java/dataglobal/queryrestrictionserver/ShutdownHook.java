/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver;

import static dataglobal.queryrestrictionserver.Server.log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author eobs
 */
public class ShutdownHook implements Runnable{

     static Logger log = LogManager.getLogger(ShutdownHook.class.getName());

    public ShutdownHook() {
        log.info("add shutdown hook Instance");
    }
     
     
     
      public void run() {
                  WindowsEventLogWriter.fireWindowsEvent(WindowsEventLogWriter.WARNING,WindowsEventLogWriter.SERVICE_STOPED,"Service Stoped");
                  System.out.print("Test shutdown hook");
                  log.info("shutdown hook called");
            }
    
}
