/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver.model;

/**
 *
 * @author eobs
 */
public class FieldMatching {

    public FieldMatching(boolean acces, String pattern, String memberref, String fieldname) {
        this.acces = acces;
        this.pattern = pattern;
        this.memberref = memberref;
        this.fieldname = fieldname;
    }

    public boolean isAcces() {
        return acces;
    }

    public void setAcces(boolean acces) {
        this.acces = acces;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }
    
    private boolean acces;
    private String pattern;
    private String memberref;

    public String getMemberref() {
        return memberref;
    }

    public void setMemberref(String memberref) {
        this.memberref = memberref;
    }
    private String fieldname;
    
}
