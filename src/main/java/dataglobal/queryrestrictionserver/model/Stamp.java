/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.queryrestrictionserver.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 *
 * @author eobs
 */
public class Stamp {
    
    public static final int OR=1;
    public static final int AND=2;
    
    private String pattern;
    
    public void setPattern(String patternString)
    {
       pattern=patternString;
        
    }
    
    public boolean isPatternMatching(String stampName)
    {
        if (pattern==null) return false;
        return stampName.matches(pattern);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getFields() {
        return fields;
    }

    public void setFields(ArrayList<String> fields) {
        this.fields = fields;
    }

    public String getPlugin() {
        return plugin;
    }

    public void setPlugin(String plugin) {
        this.plugin = plugin;
    }
    
    public void setOperator(String operator )
    {
        if (operator==null)
        {
            this.operator=AND;
        }
        else if (operator.trim().equalsIgnoreCase("OR"))
        {
            this.operator=OR;
        }
        else
        {
             this.operator=AND;
        }
        
    }
    
    public int getOperator()
    {
        return this.operator;
    }
    
    public HashMap<String, String> getFieldMapping() {
        return fieldMapping;
    }

    public void setFieldMapping(HashMap<String, String> fieldMapping) {
        this.fieldMapping = fieldMapping;
    }
    
    public void addFieldMatching(String field,String pattern,boolean access,String memberRef)
    {
        this.fieldMatching.add(new FieldMatching(access, pattern, memberRef, field));
    }
    
    public ArrayList<FieldMatching> getFieldMatching()
    {
        return this.fieldMatching;
    }
    private String name;
    private ArrayList<String> fields;
    private String plugin;
    private int operator=AND;
    private HashMap<String,String> fieldMapping=new HashMap<>();
    private ArrayList<FieldMatching> fieldMatching=new ArrayList<FieldMatching>();

   
}
