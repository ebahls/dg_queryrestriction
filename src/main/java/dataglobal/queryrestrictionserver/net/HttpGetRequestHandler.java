/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver.net;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 *
 * @author eobs
 */
public class HttpGetRequestHandler implements Runnable {

     static  Logger log = LogManager.getLogger(HttpGetRequestHandler.class.getName());

    private HttpRequest handler;
    private Socket socket;
    private HashMap<String, String> requestMapping;

    //private HashMap<String,String> requestMap;
    public HttpGetRequestHandler(Socket socket, HttpRequest requestHandler) {
        this.handler = requestHandler;
        this.socket = socket;
        this.requestMapping = new HashMap<String, String>();
    }

    @Override
    public void run() {

        try {
            if (parseHeader()) {
                handler.response(requestMapping, this);
                 
            } else {

                socket.close();
            }

        } catch (Exception ex) {
            log.error("Request handle Error", ex);
        }

    }

    private boolean parseHeader() throws Exception {
        log.debug("Parsing the HTML header");

        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line;
    
        int lineNumber = 0;
        while ((line = br.readLine()) != null) {

            if (line.trim().length() == 0) {
                break;
            }

            if (line.startsWith("POST")) {
                log.warn("post is not supportet");
                return false;
            }

            if (line.startsWith("GET")) {
                String test=line.split("\\?")[0];
                log.debug("req Application:"+test);
                log.debug("Acceptet "+test.endsWith("rest.api"));
                
                if (!line.split("\\?")[0].endsWith("rest.api")) {

                    log.error("Application not aceptet just rest.api is acceptet");
                    errorResponse(500);
                    return false;

                }
                parseRequestParameter(line);
                log.debug("Request Type GET");

            }

            if (line.startsWith("Host")) {
                String host = line.split(":")[1];
                this.requestMapping.put("host", host);
                log.debug("Request from:" + host);
            }

            lineNumber++;
            log.debug(lineNumber + " : " + line);
        }
        return handler.accept(requestMapping,this);
    }

    private void parseRequestParameter(String getLine) {
        String requests = getLine.split("\\?")[1];

        // check the application request
        //cleanup the HTTP** part
        int li = requests.lastIndexOf("HTTP");

        requests = requests.substring(0, li).trim();

        String parameters[] = requests.split("&");

        for (String parameter : parameters) {
            String propValue[] = parameter.split("=");

            this.requestMapping.put(propValue[0].trim(), propValue[1].trim());

            log.debug("Found Parameter:" + parameter);

        }

    }

    public void errorResponse(int httpErrNr) throws IOException {
        log.info("Response Error:" + httpErrNr);

        PrintStream respOut = new PrintStream(socket.getOutputStream());

        respOut.print("HTTP/1.1 " + httpErrNr + " Error\r\n");

        respOut.print("\r\n");

        socket.close();

        log.info("Connection Closed");
    }

    public void response(ResponseObject responseObject)   {
        log.debug("Response Result Object");
       responseObject.buildXML();
       try 
       {
        PrintStream httpOut = new PrintStream(socket.getOutputStream());

        httpOut.print("HTTP/1.1 200 OK\r\n");

        httpOut.print("Content-Type: application/xml \r\n"); 
        httpOut.print("Content-Length: " + responseObject.getContentLengt() + "\r\n");

        httpOut.print("\r\n");

        socket.getOutputStream().write(responseObject.getContent());

        socket.close();

        log.debug("Connection Closed");
       }
       catch(IOException ex)
       {
           log.debug("Socket Closed by Client cant send response");
       }
       
    }

}
