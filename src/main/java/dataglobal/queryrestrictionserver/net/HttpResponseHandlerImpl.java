/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver.net;

import com.sun.net.httpserver.HttpExchange;
import dataglobal.dataroomauthorisation.model.QueryRestrictionResult;
import dataglobal.queryrestrictionserver.QueryRestrictionPlugin;
import dataglobal.queryrestrictionserver.QueryRestrictionStampSetup;
import dataglobal.queryrestrictionserver.ServerFactory;
import dataglobal.queryrestrictionserver.ServerStatistics;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author eobs
 */
public class HttpResponseHandlerImpl implements  Runnable {

    static Logger log = LogManager.getLogger(OracleHttpServer.class.getName());
    ServerFactory conf;
    HashMap<String, String> requestMap;
      long SID = 0;
    HttpExchange httpExchange;
    
    

    public HttpResponseHandlerImpl(HttpExchange httpExchange,long sid ) {
         this.httpExchange=httpExchange;
         SID=sid;
         conf=ServerFactory.getInstance();
    }

    private void parseGetRequest(HttpExchange he) {
        requestMap = new HashMap<String, String>();
        String query = he.getRequestURI().getQuery();
        String parameters[] = query.split("&");

        for (String parameter : parameters) {
            String propValue[] = parameter.split("=");

            this.requestMap.put(propValue[0].trim(), propValue[1].trim());

            log.debug("SID :"+SID+" Found Parameter:" + parameter);

        }
    }

    
   

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        try{
          parseGetRequest(this.httpExchange);
        log.debug("SID :"+SID+" before call QR factory");
        QueryRestrictionPlugin plugin = this.conf.getPlugin(SID, requestMap.get("domain"), requestMap.get("user"), requestMap.get("stamp"));
        log.debug("SID :"+SID+" after calling QR factory");
        

        ArrayList<String> fields = new ArrayList<>();

        String fieldString = requestMap.get("fields");
        if (fieldString.contains(",")) {
            String unparsedFields[] = fieldString.split(",");
            for (String field : unparsedFields) {
                log.debug("Request stamp setup add field:" + field);
                fields.add(field);
            }
        } else {
            fields.add(fieldString);
        }

        QueryRestrictionStampSetup setup = plugin.getStampSetup(fields);
        QueryRestrictionResult result = new QueryRestrictionResult();

        result.setEnabled(plugin.hasQueryRestriction());

        result.setSid(SID);
        result.setID(requestMap.get("id"));

        if (plugin.hasQueryRestriction()) {
            for (String f : setup.getStampFields()) {
                Set<String> values = plugin.getAllowedValues(f);

                if (values != null) {
                    result.setAllowedValue(f, values);
                }
            }
        }

        if (plugin.getImpliciteOperator() != null) {
            result.setFieldoperator(plugin.getImpliciteOperator());
        } else {
            result.setFieldoperator(setup.getOpperator());
        }

        result.setFieldMatchingValues(plugin.getFieldMatchingDescription());
        
        // tear down plugin
        result.buildXML();
        plugin.tearDown();

        if (ServerFactory.getInstance().hasProtocollObfuscating())
        {
            
            Base64.Encoder encoder = Base64.getEncoder();
            byte[] buffer=encoder.encode(result.getContent());
            this.httpExchange.getResponseHeaders().add("Content-Type", "application/base64;charset=utf-8");   
            this.httpExchange.sendResponseHeaders(200, buffer.length);
            OutputStream os = this.httpExchange.getResponseBody();
            os.write(buffer);
            
             os.close();
              this.httpExchange.close();
            
        }
        else
        {
        this.httpExchange.getResponseHeaders().add("Content-Type", "application/xml;charset=utf-8");  
        
        this.httpExchange.sendResponseHeaders(200, result.getContentLengt());
        OutputStream os = this.httpExchange.getResponseBody();
        
        
            os.write(result.getContent());
             os.close();
             this.httpExchange.close();
        }
        
             long remain = System.currentTimeMillis() - start;
        log.info("Request with SID:" + SID + " took " + remain + "ms");
        ServerStatistics.countRequest(remain, result.getContentLengt());
        log.info("Request with SID:" + SID + " finish");
        }
        catch (IOException ex)
        {   log.error(" SID:" + SID + " IO Error");
            log.error(ex);
        }
  
    }

}
