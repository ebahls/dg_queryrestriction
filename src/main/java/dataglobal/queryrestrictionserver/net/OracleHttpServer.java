/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataglobal.queryrestrictionserver.net;

import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;
import dataglobal.queryrestrictionserver.ServerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.Executors;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.common.util.impl.LoggerFactory;

/**
 *
 * @author eobs
 */
public class OracleHttpServer {

    static Logger log = LogManager.getLogger(OracleHttpServer.class.getName());

    public OracleHttpServer(boolean https, String context, int port, ServerFactory conf) throws IOException, KeyStoreException, FileNotFoundException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {

        log.info("using Oracle http server implementaition");
        int workerthreads=15;
        int socketbacklog= 10;
        
         if (conf.getServerConfigProperty("socketbacklog")!=null)
        {
            workerthreads=Integer.parseInt(conf.getServerConfigProperty("socketbacklog"));
        }
        
        if (conf.getServerConfigProperty("workerthreads")!=null)
        {
            workerthreads=Integer.parseInt(conf.getServerConfigProperty("workerthreads"));
        }
        
        
        log.info("Webservice Workerthread pool size :"+workerthreads);
        log.info("Webservice socket backlog size :"+socketbacklog);
        
        if (https) {
            log.info("url: https://<host>:" + port + context);
            HttpsServer server = HttpsServer.create(new InetSocketAddress(port), socketbacklog);
            server.createContext(context, new HttpResponseHandler(conf));
            server.setHttpsConfigurator(new HttpsConfigurator(getSSLContext(conf)) {
                public void configure(HttpsParameters params) {
                    try {

                        InetSocketAddress remote = params.getClientAddress();
                        log.info("SSL config :" + remote.getHostName());
                        SSLContext c = getSSLContext();

                        // get the default parameters
                        SSLParameters sslparams = c.getDefaultSSLParameters();

                        params.setWantClientAuth(false);
                        params.setNeedClientAuth(false);

                        params.setSSLParameters(sslparams);
                        /*
                         // initialise the SSL context
                         SSLContext c = SSLContext.getDefault ();
                         SSLEngine engine = c.createSSLEngine ();
                         params.setNeedClientAuth ( false );
                         params.setCipherSuites ( engine.getEnabledCipherSuites () );
                         params.setProtocols ( engine.getEnabledProtocols () );

                         // get the default parameters
                         SSLParameters defaultSSLParameters = c.getDefaultSSLParameters ();
                         params.setSSLParameters ( defaultSSLParameters );
                         */
                    } catch (Exception ex) {

                        log.trace(ex);
                        log.error("Failed to create HTTPS port");
                    }
                }
            });
            //server.setExecutor(Executors.newWorkStealingPool());
             server.setExecutor(Executors.newFixedThreadPool(workerthreads));
            server.start();
            log.info("https Server ready");

        } else {
            log.info("url: http://<host>:" + port + context);
            HttpServer server = HttpServer.create(new InetSocketAddress(port), socketbacklog);
            server.createContext(context, new HttpResponseHandler(conf));
            //server.setExecutor(Executors.newWorkStealingPool());
             server.setExecutor(Executors.newFixedThreadPool(workerthreads));
              
            server.start();
           
            log.info("http Server ready");

        }

    }

    private SSLContext getSSLContext(ServerFactory conf) throws KeyStoreException, FileNotFoundException, NoSuchAlgorithmException, IOException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        log.info("Init SSL Context");

        SSLContext sslContext = SSLContext.getInstance("TLS");

         // initialise the keystore
        File keystoreFile = new File(new File(conf.getServerConfigProperty("keypath")), conf.getServerConfigProperty("https.keystore"));
        log.info("Load keystore :" + keystoreFile.getAbsolutePath());
        char[] password = conf.getServerConfigProperty("https.pharase").toCharArray();
        KeyStore ks = KeyStore.getInstance("JKS");
        FileInputStream fis = new FileInputStream(keystoreFile);

        ks.load(fis, password);

        // setup the key manager factory
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, password);

        // setup the trust manager factory
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(ks);
        sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        return sslContext;

    }

}
