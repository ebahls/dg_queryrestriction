/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.queryrestrictionserver.net;

import java.io.IOException;

import java.util.HashMap;


/**
 *
 * @author eobs
 */
public interface HttpRequest {
    
     boolean accept(HashMap<String,String> requestMap,HttpGetRequestHandler handler) throws IOException;
    
     void response(HashMap<String,String> requestMap,HttpGetRequestHandler handler) throws IOException;
    
}
