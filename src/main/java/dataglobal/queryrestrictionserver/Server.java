/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.queryrestrictionserver;

 
import com.jcabi.manifests.Manifests;
import dataglobal.queryrestrictionserver.net.OracleHttpServer;
import dataglobal.queryrestrictionserver.security.SignatureHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.JDOMException;


/**
 *
 * @author eobs
 */
public class Server {

    /**
     * @param args the command line arguments
     */
    
    static  Logger log ;
    static boolean serviceStarted=false;
   
    
    
    public static void start(String[] args)  
    {
        
        if (!serviceStarted)
        {
        try 
        {
            Server.main(args);
        }catch(IOException | JDOMException | ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException ex)
        {
        log.error(ex);
        }   catch (KeyStoreException | CertificateException | UnrecoverableKeyException | KeyManagementException ex) {
                log.error(ex);
            }
        }
        
    }
    
      public static void stop(String[] args)
    {
        serviceStarted=false;
        log.info("Service Stoped");
        Runtime.getRuntime().halt(0);
        
        
    }
    
    
      
    public static void main(String[] args) throws IOException, JDOMException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException, NoSuchProviderException, FileNotFoundException, InvalidKeySpecException, KeyStoreException, CertificateException, UnrecoverableKeyException, KeyManagementException {
       
        serviceStarted=true;
        String version="In build environment";
         File confPath=new File("conf");
        
          // init looging System
        System.setProperty("log4j.configurationFile", new File(confPath,"/log4j2.xml").toURI().toString());
        System.setProperty("org.jboss.logging.provider", "slf4j");
     
        System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
        
        //Log in console in and log file
        log = LogManager.getLogger(Server.class.getName());
        Logger acclog=LogManager.getLogger("access");
        
         if (Manifests.exists("PROJECT-VERSION")) 
         {
                version=Manifests.read("PROJECT-VERSION");
         }
      
         
        log.info("Start Server version:"+version);
        
        // implement shutdown hook
        
       Runtime.getRuntime().addShutdownHook(new Thread(new ShutdownHook()));
         
        
        log.info("Loking for :"+confPath.getAbsolutePath());
        acclog.info("Start Server");
       
        ServerFactory conf=new ServerFactory(new File(confPath,"ModelConfiguration.xml"));
         
        SignatureHelper.initSecurity(new File(conf.getServerConfigProperty("keypath")));
     
        //ServerSocket serversock=new ServerSocket(Integer.valueOf(conf.getServerConfigProperty("port")));
        
        ServerFactory.getInstance().addNewJob(conf.getServerConfigProperty("serverstatistics.cron"), "dataglobal.queryrestrictionserver.ServerStatistics", "ServerStatistics", null);
        
        // If keep alive config availabil do a setup setup 
        String keepAliveCron=conf.getServerConfigProperty("keepalive.cron");
        if (keepAliveCron!=null)
        {
            ServerFactory.getInstance().addNewJob(keepAliveCron,"dataglobal.queryrestrictionserver.WindowsEventLogWriter","KeepAlive",null);
        }
                
        boolean https=false;
        
        if (conf.getServerConfigProperty("protocoll").compareToIgnoreCase("https")==0) https=true;
        
         new OracleHttpServer(https, "/rest.api", Integer.valueOf(conf.getServerConfigProperty("port")), conf);
        
       
        /**
        //serversock.setSoTimeout(10000);
        while (serviceStarted)
        {
        log.debug("Waiting for connection");
        Socket client=serversock.accept();
        //client.setSoTimeout(5000);
        //CommunicationHandler com=new CommunicationHandler(client, conf);
        if (!serviceStarted) 
        {
        log.info("Exit requestet");
        break;
        }
        HttpGetRequestHandler handler=new HttpGetRequestHandler(client,new QueryRestrictionHttpRequest(conf) );
        //acclog.info("Client Connected: "+client.getRemoteSocketAddress().toString()+" SID:"+com.getSessionID());
        new Thread(handler).start();
        // manager.addCommunicationHandler(com);
        // new Thread(com ).start();
        }
         * **/

        
    }
    
   
    
}
