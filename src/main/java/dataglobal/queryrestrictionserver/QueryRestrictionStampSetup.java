/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataglobal.queryrestrictionserver;

import dataglobal.queryrestrictionserver.model.Stamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author eobs
 */

public class QueryRestrictionStampSetup {
    
    public static int STAMP_NOT_SUPPORTET=10;
    public static int QUERY_RESTRICTION_DISABLED=20;
    public static int QUERY_RESTRICTION_ENABLED=30;
    
    private Stamp stamp;
    
    private ArrayList<Integer> usedFields=new ArrayList<Integer>();
    private int status=10;
    
    public void setUsedStampFields(ArrayList<String> availableFields, dataglobal.queryrestrictionserver.model.Stamp stamp)
    {
        this.stamp=stamp;
        int mandatoryCount=stamp.getFields().size();
        
        for (String mandetoryField:stamp.getFields())
        {
            int count=0;
            for (String availableField:availableFields)
            {
                if (availableField.compareToIgnoreCase(mandetoryField)==0)
                {
                    this.usedFields.add(count);
                    
                    mandatoryCount--;
                }
                count++;
                    
            }
        }
        
        if (mandatoryCount>0)
        {
            this.status=STAMP_NOT_SUPPORTET;
        }
        else
        {
            this.status=QUERY_RESTRICTION_ENABLED;
           
        }
        
    }
    
    public void disableQueryRestriction()
    {
        this.status=QUERY_RESTRICTION_DISABLED;
    }
    
     public void stampNotSupportet()
    {
        
        this.status=STAMP_NOT_SUPPORTET;
    }
    @Deprecated
    public ArrayList<Integer> getUsedStampFields()
    {
        return this.usedFields;
        
    }
    
    public int getStatus()
    {
        return this.status;
    }
    
    public String getOpperator()
    {
        if (this.stamp==null)return "AND";
        if (this.stamp.getOperator()==Stamp.AND) return "AND";
        
        return "OR";
    }
    
    public Set<String> getStampFields()
    {
        if (this.stamp==null) return new HashSet<String>();
        return new HashSet<String>(this.stamp.getFields());
    }
    
    
    
     
    
}
