@echo off
set INST_PATH=

set SERVICE_NAME=QueryRestriction
set PR_DESCRIPTION=QueryRestriction Service



set PR_INSTALL=%INST_PATH%\service\QueryRestriction.exe
REM Service log configuration
set PR_LOGPREFIX=%SERVICE_NAME%
set PR_LOGPATH=%INST_PATH%\log
set PR_STDOUTPUT=%INST_PATH%
set PR_STDERROR=%INST_PATH%
set PR_STARTPATH=%INST_PATH%
set PR_LOGLEVEL=Error
 
REM Path to java installation
set JVMPATH=%INST_PATH%\jre8\bin\
set PR_JVM=%JVMPATH%server\jvm.dll
set PR_CLASSPATH=%INST_PATH%\QueryRestrictionServer.jar
 
REM Startup configuration
set PR_STARTUP=auto
set PR_STARTMODE=jvm
set PR_STARTCLASS=dataglobal.queryrestrictionserver.Server
set PR_STARTMETHOD=start
 
REM Shutdown configuration
set PR_STOPMODE=jvm
set PR_STOPCLASS=dataglobal.queryrestrictionserver.Server
set PR_STOPMETHOD=stop
 
REM JVM configuration
set PR_JVMMS=256
set PR_JVMMX=1024
set PR_JVMSS=4000
set PR_JVMOPTIONS=-Duser.language=DE;-Duser.region=de
 
QueryRestriction.exe //IS  



