;NSIS Modern User Interface
;Basic Example Script
;Written by Joost Verburg

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;General

  ;Name and file
  Name "QueryRestrictionServer"
  OutFile "QueryRestrictionServer.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\QueryRestrictionServer"
  
  ;Get installation folder from registry if available
   

  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

   
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "QueryRestrictionServer" SecDummy

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
    File /r "..\InstallService\*.*"
  ;Store installation folder
  WriteRegStr HKCU "Software\DataGlobal\QueryRestrictionServer" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  ClearErrors
FileOpen $0 "$INSTDIR\service\install.bat" "r"                     ; open target file for reading
GetTempFileName $R0                            ; get new temp file name
FileOpen $1 $R0 "w"                            ; open temp file for writing
loop:
   FileRead $0 $2                              ; read line from target file
   IfErrors done                               ; check if end of file reached
   StrCmp $2 "set INST_PATH=$\r$\n" 0 +2      ; compare line with search string with CR/LF
      StrCpy $2 "set INST_PATH=$INSTDIR$\r$\n"    ; change line
   StrCmp $2 "set INST_PATH=" 0 +2            ; compare line with search string without CR/LF (at the end of the file)
      StrCpy $2 "set INST_PATH=$INSTDIR"          ; change line
   FileWrite $1 $2                             ; write changed or unchanged line to temp file
   Goto loop
 
done:
   FileClose $0                                ; close target file
   FileClose $1                                ; close temp file
   Delete "file.txt"                           ; delete target file
   CopyFiles /SILENT $R0 "$INSTDIR\service\install.bat"            ; copy temp file to target file
   Delete $R0                                  ; delete temp file
  
  ExecWait "$INSTDIR\service\install.bat"

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecDummy ${LANG_ENGLISH} "A test section."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
   !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
  ExecWait "$INSTDIR\service\uninstall.bat"
  RMDir /r "$INSTDIR"

  

  DeleteRegKey /ifempty HKCU "Software\DataGlobal\QueryRestrictionServer"

SectionEnd

